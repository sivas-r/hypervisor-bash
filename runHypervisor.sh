#!/bin/bash

#Script to run Xen Hypervisor in SoPC.

#Bare metal app binary and cfg
app01_bin=hyper_bare.bin
app01_cfg=BM_Hypervisor.cfg

#Copy images and cfg from SD card.
cp /mnt/Image /boot
cp /mnt/rootfs.cpio.gz /boot
cp /mnt/$app01_bin /etc/xen
cp /mnt/$app01_cfg /etc/xen

#Create instance of app or OS.
Create() {
    echo "######################## Creation ########################"
    echo "Enter your choice: "
    echo "1. Bare Metal App."
    echo "2. OS Instance."
    echo "##########################################################"
    read option

    if [ $option = 1 ] ; then
        xl create $app01_cfg
    elif [ $option = 2 ] ; then
        echo "Name of the Instance? "
        read name
        echo "Which core?"
        read cpuNum
        xl create -c example-simple.cfg 'name='\"$name\" 'vcpus="1"' 'cpus='$cpuNum 'extra="console=hvc0 rdinit=/sbin/init root=/dev/ram0 rw rootwait"' 'ramdisk="/boot/rootfs.cpio.gz"'
   fi
}

#Destroy instance.
Destroy() {
    echo "######################## Destroy #########################"
    echo "Which instance to kill?"
    xl vcpu-list
    echo "##########################################################"
    read name
    xl destroy $name
}

#User interaction to create or destroy app/OS instances.
CreateOrDestroy() {
    xl vcpu-list
    echo "##################### Xen Hypervisor #####################"
    echo "Enter your choice: "
    echo "1. Create instance."
    echo "2. Destroy."
    echo "3. Exit."
    echo "##########################################################"
    read option

    if [ $option =  1 ] ; then
        Create;
    elif [ $option = 2 ] ; then
        Destroy;
    fi

    if [ $option != 3 ] ; then
    	CreateOrDestroy;
    fi
}

#Main routine call
CreateOrDestroy;
